﻿using System;
class Addnumbers{
 
  
  static bool checkthePair(int [] A, int size, int x) {
      for (int i = 0; i < (size - 1); i++) {
          for (int j = (i + 1); j < size; j++) {
              if (A[i] + A[j] == x) {
                  Console.WriteLine("Sum of the pair " + x +
                                     " is (" + A[i] + ", " + A[j] + ")");
   
 
                  return true;
              }
          }
      }
 
      return false;
  }
 
  public static void Main()
  {
      int [] A = {10,15,3,7};
      int x = 17;
      int size = A.Length;
 
      if (checkthePair(A, size, x)) {
          Console.WriteLine("True");
      }
      else {
          Console.WriteLine("False " + x );
      } 
  }
}